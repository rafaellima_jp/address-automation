/*Created by Rafael Anderson*/

'use strict';

const UserPage = function() {

    //Bloco de Variáveis
    var loginData = require ('../login/login.data'),
        pageLogin = require('../login/login.po'),
        helper = require('../util/helper');
    var login = loginData.userAdmin();

    //Bloco de constantes
    const btn_addressesMenu = element(by.css('#navbar > div.navbar-nav.mr-auto > a:nth-child(2)'));
    const btn_listCreate = element(by.css('body > div > div.row.justify-content-center > a:nth-child(2)'));
    const btn_newAddress = element(by.css('body > div > a'));
    const field_firstNameCreate = element(by.id('address_first_name'));
    const field_lastNameCreate = element(by.id('address_last_name'));
    const field_adress1Create = element(by.id('address_street_address'));
    const field_adress2Create = element(by.id('address_secondary_address'));
    const field_adressCityCreate = element(by.id('address_city'));
    const field_adressStateCreate = element(by.id('address_state'));
    const field_countryUsCreate = element(by.id('address_country_us'));
    const field_countryCaCreate = element(by.id('address_country_canada'));
    const field_adressBirthdayCreate = element(by.id('address_birthday'));
    const field_adressAgeCreate = element(by.id('address_age'));
    const field_adressWebsiteCreate = element(by.id('address_website'));
    const field_adressPhoneCreate = element(by.id('address_phone'));
    const field_adressZipCreate = element(by.id('address_zip_code'));
    const label_sucessMessage = element(by.css('body > div > div.alert.alert-notice'));
    const btn_createAdrdress = element(by.css('#new_address > div.form-group.row.justify-content-center > input'));
    const btn_editAddress = element(by.css('body > div > table > tbody > tr:nth-child(1) > td:nth-child(6) > a'));
    const field_firstNameTable = element(by.css('body > div > p:nth-child(2) > span.col.offset-1.offset-sm-0'));
    const btn_updateAdrdress = element(by.buttonText('Update Address'));
    const btn_listAdrdress = element(by.css('body > div > div.row.justify-content-center > a:nth-child(2)'));
    const btn_deleteAdrdress = element(by.css('body > div > table > tbody > tr:nth-child(1) > td:nth-child(7) > a'));
    const label_sucessMessageDelete = element(by.css('body > div > div'));
    const btn_editCreateAdress = element(by.css(' body > div > div.row.justify-content-center > a:nth-child(1)'));

    //Bloco de Funções
    return{
            getSucessMessage : function () {
                return label_sucessMessage.getText();
            },

            getSucessMessageDelete : function () {
                return label_sucessMessageDelete.getText();
            },

            getFirstNameTable  : function () {
                browser.sleep(500);
                return field_firstNameTable.getText();
            },

            clickBtnEditCreate : function() {
                btn_editCreateAdress.click();
            },

            clickBtnDelete : function() {
                btn_deleteAdrdress.click();
            },

            clickBtnListCreate : function() {
                btn_listCreate.click();
            },

            clickEditAddresse : function() {
                btn_editAddress.click();
            },

            clickListAddresse : function() {
                btn_listAdrdress.click();
            },


        clickUpdateAddresse : function() {
                browser.sleep(500);
                btn_updateAdrdress.click();
            },

            clickBtnAddressesMenu : function() {
                btn_addressesMenu.click();
            },

            clickBtnNewAddress : function() {
                btn_newAddress.click();
            },

            setfirstName : function (nameFirst) {
                browser.sleep(500);
                field_firstNameCreate.sendKeys(nameFirst);
            },

            setSecondName : function (nameSecond) {
                browser.sleep(500);
                field_lastNameCreate.sendKeys(nameSecond);
            },

            setAddress1 : function (addressOne) {
                browser.sleep(500);
                field_adress1Create.sendKeys(addressOne);
            },

            setAddress2 : function (addressTwo) {
                browser.sleep(500);
                field_adress2Create.sendKeys(addressTwo);
            },

            setAdressCityCreate : function (city) {
                browser.sleep(500);
                field_adressCityCreate.sendKeys(city);
            },

            setAdressStaetCreate : function (state) {
                field_adressStateCreate.sendKeys(state);
            },

            setCountryCreate : function (us) {
                if(us == 'United States'){
                    field_countryUsCreate.click();
                }else {
                    field_countryCaCreate.click();
                }
            },

            setAdressBirthdayCreate : function (birthday) {
                browser.sleep(500);
                field_adressBirthdayCreate.sendKeys(birthday);
            },

            setAdressAgeCreate : function (age) {
                browser.sleep(500);
                field_adressAgeCreate.sendKeys(age);
            },

            setAdressSiteCreate : function (site) {
                browser.sleep(500);
                field_adressWebsiteCreate.sendKeys(site);
            },

            setAdressPhoneCreate : function (phone) {
                browser.sleep(500);
                field_adressPhoneCreate.sendKeys(phone);
            },
            setZipCreate : function (zip) {
                browser.sleep(500);
                field_adressZipCreate.sendKeys(zip);
            },
            clickBtnCreateAddress : function() {
                btn_createAdrdress.click();
            },

        acessMenuEndereco :  function () {
                browser.get('/');
                pageLogin.runLogin(login.username, login.password);
                browser.sleep(800);
                this.clickBtnAddressesMenu();
            },

            acessFormNewEndereco :  function () {
                browser.sleep(800);
                this.clickBtnNewAddress();
            },

            setFieldsNewUser : function (name1, name2, address1, address2, city, state, country,
                    zip, birthday, age, site, phone) {
                browser.sleep(800);
              //validar preenchimento do campo código caso ele seja vazio
              if(name1 !== ' '){
                this.setfirstName(name1);
              }
              if(name2 !== ' '){
                this.setSecondName(name2);
              }
              if(address1 !== ' '){
                this.setAddress1(address1);
              }
              if(address2 !== ' '){
                this.setAddress2(address2);
              }
              browser.sleep(500);
              if(city !== ' '){
                this.setAdressCityCreate(city);
              }
              //
              this.setAdressStaetCreate(state);
              this.setCountryCreate(country);
              //
                if(birthday !== ' '){
                    this.setAdressBirthdayCreate(birthday);
                }
                if(age !== ' '){
                    this.setAdressAgeCreate(age);
                }
                if(site !== ' '){
                    this.setAdressSiteCreate(site);
                }
                if(phone !== ' '){
                    this.setAdressPhoneCreate(phone);
                }
                if(zip !== ' '){
                    this.setZipCreate(zip);
                }
              this.clickBtnCreateAddress();
                browser.sleep(800);
            },

                searchNameInTable : function (name1) {
                var grid = $$('body > div > table');
                grid.each(function(row) {
                    var rowElems = row.$$('td');
                    for (var i = 0; i<=rowElems.count(); i++) {
                        if(rowElems.get(i).getText() === name1){
                            return true
                        }
                    }
                    return false;
                });
            },

        setFieldsEditUser : function (name1) {
            helper.clearField(field_firstNameCreate, 255);
            //validar preenchimento do campo código caso ele seja vazio
            if (name1 !== ' ') {
                this.setfirstName(name1);
            }
            browser.sleep(800);
        }
    }
};

module.exports = new UserPage();


