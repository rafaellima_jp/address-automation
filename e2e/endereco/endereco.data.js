/*Created by Rafael Anderson*/

'use strict';
var helper = require('../util/helper.js');
var enderecoData = function() {


    return {
        user: function () {
            //var fullName = helper.getText(8);
            var code = helper.getValorLimitadoParametro(123456, 999999);
            return {
                name1: helper.getText(8),
                name2: helper.getText(8),
                address1: helper.getText(8),
                address2: helper.getText(8),
                city: helper.getText(8),
                state: 'Arkansas',
                zip: code,
                country: 'Canada',
                birthday:'23031994',
                age: 25,
                website: 'http://www.teste.com',
                phone: '996283871',
                nameEdit1: helper.getText(8)
            }
        }
    }
};

module.exports = new enderecoData();
