/*Created by Rafael Anderson*/

'use strict';

browser.ignoreSynchronization = true;

describe('Endereco', function () {

    var pageEndereco = require('./endereco.po'),
        pageLogin = require('../login/login.po'),
        enderecoData = require ('./endereco.data');
    var endereco;

    beforeEach(function () {
        endereco = enderecoData.user();
        pageEndereco.acessMenuEndereco();
        browser.sleep(600);

      });

    afterEach(function() {
        pageLogin.runLogout();
    });

    it('Deletar endereco com sucesso', function () {
        //Insersação dos dados no formulário
        browser.sleep(800);
        pageEndereco.clickBtnDelete();
        browser.sleep(400);
        browser.switchTo().alert().accept();
        expect(pageEndereco.getSucessMessageDelete()).toBe('Address was successfully destroyed.');

    });
});
