/*Created by Rafael Anderson*/

'use strict';

browser.ignoreSynchronization = true;

describe('Endereco', function () {

    var pageEndereco = require('./endereco.po'),
        pageLogin = require('../login/login.po'),
        enderecoData = require ('./endereco.data');
    var endereco;

    beforeEach(function () {
        endereco = enderecoData.user();
        pageEndereco.acessMenuEndereco();
        browser.sleep(800);

      });

    afterEach(function() {
        pageLogin.runLogout();
    });

    it('Editar endereco com sucesso', function () {
        //Insersação dos dados no formulário
        browser.sleep(800);
        pageEndereco.acessFormNewEndereco();
        //Validar criação de Endereços com dados válidos
        pageEndereco.setFieldsNewUser(endereco.name1, endereco.name2, endereco.address1, endereco.address2, endereco.city,
            endereco.state, endereco.country, endereco.zip, endereco.birthday, endereco.age, endereco.website,
            endereco.phone);
        browser.sleep(800);
        pageEndereco.clickBtnEditCreate();
        browser.sleep(800);
        pageEndereco.setFieldsEditUser(endereco.nameEdit1);
        pageEndereco.clickUpdateAddresse();
        browser.sleep(200);
        expect(pageEndereco.getFirstNameTable()).toBe(endereco.nameEdit1);
    });
});
