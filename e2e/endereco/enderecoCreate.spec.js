/*Created by Rafael Anderson*/

'use strict';

browser.ignoreSynchronization = true;

describe('Endereco', function () {

    var pageEndereco = require('./endereco.po'),
        pageLogin = require('../login/login.po'),
        enderecoData = require ('./endereco.data');
    var endereco;

    beforeEach(function () {
        endereco = enderecoData.user();
        pageEndereco.acessMenuEndereco();
        browser.sleep(200);

      });

    afterEach(function() {
        pageLogin.runLogout();
    });

    it('Cadastrar endereco com sucesso', function () {
        //Insersação dos dados no formulário
        browser.sleep(600);
        pageEndereco.acessFormNewEndereco();
        //Validar criação de Endereços com dados válidos
        pageEndereco.setFieldsNewUser(endereco.name1, endereco.name2, endereco.address1, endereco.address2, endereco.city,
            endereco.state, endereco.country, endereco.zip, endereco.birthday, endereco.age, endereco.website,
            endereco.phone);
        browser.sleep(500);
        expect(pageEndereco.getSucessMessage()).toBe('Address was successfully created.');
    });
});
