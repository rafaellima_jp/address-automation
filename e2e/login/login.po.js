/*Created by Rafael Anderson*/

'use strict';

const LoginPage = function() {


    const btn_signup = element(by.css('#clearance > div > div > form > div:nth-child(6) > a'));
    const btn_signinMenu = element(by.id('sign-in'));
    const field_email = element(by.id('user_email'));
    const field_password = element(by.id('user_password'));
    const field_emailLogin = element(by.id('session_email'));
    const field_passwordLogin = element(by.id('session_password'));
    const btn_signupEnter = element(by.css('#new_user > div:nth-child(5) > input'));
    const btn_sair = element(by.css('#navbar > div.navbar-nav.mr-auto > a:nth-child(3)'));
    const btn_login = element(by.css('#clearance > div > div > form > div:nth-child(5) > input'));

    return {
        setEmail : function(userName){
            field_email.sendKeys(userName);
        },

        setEmailLogin : function(userName){
            field_emailLogin.sendKeys(userName);
        },

        setPassword : function(password){
            field_password.sendKeys(password);
        },

        setPasswordLogin : function(password){
            field_passwordLogin.sendKeys(password);
        },

        clickBtnSignupEnter : function() {
            btn_signupEnter.click();
        },

        clickSignUp : function(){
            btn_signup.click();
        },

        clickSignInMenu : function(){
            btn_signinMenu.click();
        },

        clickLogin : function(){
            btn_login.click();
        },
        clickSair : function(){
            btn_sair.click();
        },
        runLogin : function (userName, password) {
            this.clickSignInMenu()
            browser.sleep(700);
            this.setEmailLogin(userName);
            this.setPasswordLogin(password);
            this.clickLogin();
        },
        runLogout : function (){
            browser.sleep(1000);
            this.clickSair();

        }
    }
};

module.exports = new LoginPage();



