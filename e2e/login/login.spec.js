/*Created by
Rafael Anderson*/

'use strict';

browser.ignoreSynchronization = true;

describe('Login', function () {

    var page = require('./login.po'),
        loginData = require ('./login.data');

    beforeEach(function () {
        browser.get('/');
    });

    afterEach(function() {
        page.runLogout();
    });

    it('Criar usuario com sucesso', function () {
        var user = loginData.userAdmin();
        page.clickSignInMenu();
        browser.sleep(500);
        page.clickSignUp();
        browser.sleep(700);
        page.setEmail(user.newusername);
        page.setPassword(user.newpassword);
        page.clickBtnSignupEnter();
        //
        expect(browser.getCurrentUrl()).toBe('http://a.testaddressbook.com/');
    });

    it('Realizar login com sucesso', function () {
        var user = loginData.userAdmin();
        page.runLogin(user.newusername, user.newpassword);
        //
        expect(browser.getCurrentUrl()).toBe('http://a.testaddressbook.com/');
    });

});
