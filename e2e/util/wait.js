/*Created by Rafael Anderson*/

'use strict';

const WaitFunctions = function() {

    return {
        //Wait por ID
        waitFieldId: function (id) {
            var EC = protractor.ExpectedConditions;
            let result = browser.driver.wait(() => {
                return true;
            });
            result.then((value) => {
                let product = element(by.id(id));
                browser.wait(EC.visibilityOf(product), 10000);
            }).catch('Erro');
        }
    }
};

module.exports = new WaitFunctions();
