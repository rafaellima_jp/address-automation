/*Created by Rafael Anderson*/

//Helper
module.exports = {
    //Gera valor aleatório, ex: se for colocado 10000 será gerado um numero até 1000
    getValorLimitadoParametro: function (min, max) {
        return Math.floor(Math.random() * (max - min) + min);
    },

//Texto aleatório
    getText: function textoAleatorio(tamanho) {
        var letras = 'ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
        var aleatorio = '';
        for (var i = 0; i < tamanho; i++) {
            var rnum = Math.floor(Math.random() * letras.length);
            aleatorio += letras.substring(rnum, rnum + 1);
        }
        return aleatorio;
    },

//Limpar campos
    clearField : function(elem, length){
      length = length || 100;
      var backspaceSeries = '';
      for (var i = 0; i < length; i++) {
        backspaceSeries += protractor.Key.BACK_SPACE;
      }
      elem.sendKeys(backspaceSeries);
    },

};
